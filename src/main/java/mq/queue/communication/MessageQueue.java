package mq.queue.communication;

import com.rabbitmq.client.*;
import mq.receiver.service.MailService;
import mq.receiver.service.TextService;
import mq.queue.entities.*;

import java.io.IOException;

public class MessageQueue {
    private Channel rabbitmqChannel;
    private static final String HOST = "localhost";
    private static String QUEUE = "rabbit_queue";


    private static MessageQueue messageQueue;
    private String message;

    private MessageQueue() {
        createChannel();
    }

    /**
     * Create communication channel.
     *
     * @return : new instance of Channel
     */
    private Channel createChannel() {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(HOST);
            Connection connection = factory.newConnection();
            rabbitmqChannel = connection.createChannel();
            rabbitmqChannel.exchangeDeclare(QUEUE, "fanout");

            return rabbitmqChannel;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    public static MessageQueue getInstance() {
        if (messageQueue == null) {
            messageQueue = new MessageQueue();
        }
        return messageQueue;
    }


    public void publish(String message) {
        try {
            if (message == null)
                throw new IllegalArgumentException();
            rabbitmqChannel.basicPublish(QUEUE, " ", null, message.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @return - if the communication channel is open
     */
    public boolean isOpen() {
        return rabbitmqChannel.isOpen();
    }

    /**
     * Delete all content of the queue
     */
    public void purgeQueue() {
        try {
            rabbitmqChannel.queuePurge(QUEUE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Close connection
     */
    public void close() {
        try {
            rabbitmqChannel.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Disconnect.
     */
    public void disconnect() {
        this.purgeQueue();
        this.close();
    }


    /**
     * Recieve message
     *
     * @return
     */
    public void receive(Channel channel, String queueName) throws IOException {

        Consumer consumer = new DefaultConsumer(channel) {

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                message = new String(body, "UTF-8");
                System.out.println("Received " + message);

                if (message != null) {
                    String[] parts = message.split(",");
                    String title = parts[0];
                    int year = Integer.parseInt(parts[1]);
                    double price = Double.parseDouble(parts[2]);
                    Dvd dvd = new Dvd(title, year, price);

                    TextService textService = new TextService();
                    textService.writeInFile(dvd);

                    MailService mailService = new MailService("adelaaciu@gmail.com","");
                    mailService.sendMail("adelaaciu@gmail.com","Dvd_Rabbit_Mq" + dvd.toString(),dvd.toString());
                }


            }
        };
        channel.basicConsume(queueName, true, consumer);

    }

}
