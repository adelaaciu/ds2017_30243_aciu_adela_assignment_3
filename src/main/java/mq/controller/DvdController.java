package mq.controller;

import mq.sender.MessageSender;
import mq.queue.entities.Dvd;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class DvdController extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response)   throws ServletException, IOException {

        String name = request.getParameter("dvd_name");
        String price = request.getParameter("dvd_price");
        String year = request.getParameter("dvd_year");

        Dvd dvd = new Dvd(name,Integer.parseInt(year), Double.parseDouble(price));
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();


        MessageSender messageSender = new MessageSender();
        String message = dvd.toString();
        messageSender.send(message);

        System.out.println("Message sent");

        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";

        out.println(docType +
                "<html>\n" +
                "<head><title>" + "Dvd" + "</title></head>\n" +
                "<body bgcolor = \"#f0f0f0\">\n" +
                "<h1>\"Added dvd\"</h1>" +
                "</body> " +
                "</html>"
        );

    }
}
