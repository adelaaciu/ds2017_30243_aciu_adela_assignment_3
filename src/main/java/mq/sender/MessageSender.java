package mq.sender;

import mq.queue.communication.MessageQueue;

public class MessageSender {
    private MessageQueue messageQueue;

    public  MessageSender(){
        messageQueue = MessageQueue.getInstance();
    }

    public void send(String message){
        messageQueue.publish(message);
    }

    public boolean isConnected() {
        if(messageQueue == null)
            return false;
        return messageQueue.isOpen();
    }

    public void disconnect() {
        if(messageQueue != null)
            messageQueue.close();
    }

    public void purgeQueue() {
        messageQueue.purgeQueue();
    }
}
