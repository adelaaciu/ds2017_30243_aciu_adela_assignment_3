package mq.receiver.service;

import com.rabbitmq.client.*;
import mq.queue.communication.MessageQueue;

import java.io.IOException;

public class MessageReciever {
    private Channel channel;
    private MessageQueue messageQueue;
    private static final String EXCHANGE_NAME = "rabbit_queue";
    private String queueName;
    private String message;

    public Channel getChannel() {
        return channel;
    }

    public String getQueueName() {
        return queueName;
    }

    public MessageReciever() {

        try {

            messageQueue = MessageQueue.getInstance();
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            Connection connection = factory.newConnection();
            channel = connection.createChannel();

            channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

            queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, EXCHANGE_NAME, "");
            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void handleRecieveMessage() throws IOException {
        messageQueue.receive(channel, queueName);
    }

    public void disconnect() {
        messageQueue.disconnect();
    }

    public boolean isConnected() {
        return messageQueue.isOpen();
    }

    public void purgeQueue() {
        messageQueue.purgeQueue();
    }

}
