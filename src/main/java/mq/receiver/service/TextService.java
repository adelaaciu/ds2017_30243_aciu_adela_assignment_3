package mq.receiver.service;


import mq.queue.entities.Dvd;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class TextService {

    private BufferedWriter bw;

    public void writeInFile(Dvd dvd) {
        try {

            Random randomGenerator = new Random();
            File f = new File("C:\\Users\\adela\\Documents\\CTI IV\\SEM I\\SD\\Teme\\DS2017_30243_Aciu_Adela_Assignment_1\\src\\main\\java\\mq\\dvd" + randomGenerator.nextInt(12506) + ".txt");
            FileWriter fw = new FileWriter(f);
            bw = new BufferedWriter(fw);
            bw.write(dvd.toString());

            System.out.println("Done");

        } catch (IOException e) {

            e.printStackTrace();
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
