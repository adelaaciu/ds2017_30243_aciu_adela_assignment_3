package mq.receiver.service;

import mq.queue.entities.Dvd;

import java.util.ArrayList;
import java.util.List;

public class DVDService {
    private List<Dvd> dvds;

    public DVDService() {
        getDvds();
    }

    private void createDvdList() {
        dvds = new ArrayList<Dvd>();
        Dvd dvd1 = new Dvd("Dvd1", 2016, 15);
        Dvd dvd2 = new Dvd("Dvd2", 2006, 4);
        Dvd dvd3 = new Dvd("Dvd3", 2017, 10);

        dvds.add(dvd1);
        dvds.add(dvd2);
        dvds.add(dvd3);
    }

    public void addDvd(String title, int year, double price) {
        Dvd dvd = new Dvd(title, year, price);
        dvds.add(dvd);
    }

    public List<Dvd> getDvds() {
        if (dvds == null) {
            createDvdList();
        }
        return dvds;
    }
}
